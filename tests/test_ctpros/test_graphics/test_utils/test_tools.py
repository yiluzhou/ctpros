import unittest

from ctpros.graphics.components.backend import *


class TestEntry(unittest.TestCase):
    def setUp(self):
        self.gui = tk.Tk()
        self.gui.withdraw()

    def tearDown(self):
        pass

    def test_init(self):
        var = tk.DoubleVar()
        var.set(2.5)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        self.assertEqual(entry.get(), "2.5")
        self.assertEqual(var.get(), 2.5)

    def test_ignore_nan(self):
        var = tk.DoubleVar()
        var.set(2.5)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.invalidfunction("", ",", ",", "0", "key", "1")
        self.assertEqual(entry.get(), "2.5")
        self.assertEqual(var.get(), 2.5)

    def test_set_updates_var(self):
        var = tk.DoubleVar()
        var.set(2.5)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.set(1.2)
        self.assertEqual(entry.get(), "1.2")
        self.assertEqual(var.get(), 1.2)

    def test_set_truncates_roundrules(self):
        var = tk.DoubleVar()
        var.set(2.52)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        self.assertEqual(entry.get(), "2.5")
        self.assertEqual(var.get(), 2.5)

    def test_set_truncates_notroundrules(self):
        var = tk.DoubleVar()
        var.set(2.56)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        self.assertEqual(entry.get(), "2.5")
        self.assertEqual(var.get(), 2.5)

    def test_newmax_updatesval(self):
        var = tk.DoubleVar()
        var.set(2.5)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.set_min(1)
        entry.set_max(2)
        self.assertEqual(entry.get(), "2")
        self.assertEqual(var.get(), 2)

    def test_newmin_updatesval(self):
        var = tk.DoubleVar()
        var.set(2.5)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.set_min(3)
        entry.set_max(4)
        self.assertEqual(entry.get(), "3")
        self.assertEqual(var.get(), 3)

    def test_add_leadingzero(self):
        var = tk.DoubleVar()
        var.set(1.2)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.invalidfunction("", ".", ".", "0", "key", "1")
        self.assertEqual(entry.get(), "0.")
        self.assertEqual(var.get(), 1.2)

    def test_add_outofboundsvar(self):
        var = tk.DoubleVar()
        entry = FloatRangeEntry(self.gui, minval=0, maxval=0, var=var)
        var.set(5)
        self.assertTrue(entry.get())

    def test_rangecapped_proper(self):
        var = tk.DoubleVar()
        var.set(1)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.invalidfunction("1", "10", "0", "1", "key", "1")
        self.assertEqual(entry.get(), "5")
        self.assertEqual(var.get(), 1)

    def test_rangecapped_within_mag(self):
        var = tk.DoubleVar()
        var.set(1)
        entry = FloatRangeEntry(self.gui, minval=-15, maxval=5, var=var)
        entry.invalidfunction("1", "15", "5", "1", "key", "1")
        self.assertEqual(entry.get(), "15")
        self.assertEqual(var.get(), 1)

    def test_negative_swaps_sign(self):
        var = tk.DoubleVar()
        var.set(1)
        entry = FloatRangeEntry(self.gui, minval=-5, maxval=5, var=var)
        entry.invalidfunction("-2", "-2-", "-", "3", "key", "1")
        self.assertEqual(entry.get(), "2")
        self.assertEqual(var.get(), 1)

    def test_capped_realval_max(self):
        var = tk.DoubleVar()
        var.set(1)
        entry = FloatRangeEntry(self.gui, minval=-15, maxval=5, var=var)
        entry.invalidfunction("", "10", "10", "0", "focusout", "1")
        self.assertEqual(entry.get(), "5")
        self.assertEqual(var.get(), 5)

    def test_capped_realval_min(self):
        var = tk.DoubleVar()
        var.set(1)
        entry = FloatRangeEntry(self.gui, minval=-5, maxval=15, var=var)
        entry.invalidfunction("", "-10", "-10", "0", "focusout", "1")
        self.assertEqual(entry.get(), "-5")
        self.assertEqual(var.get(), -5)

    def test_index_w_add(self):
        var = tk.DoubleVar()
        entry = FloatRangeEntry(self.gui, minval=0, maxval=500, var=var)
        entry.invalidfunction("1", "100", "00", "1", "focusout", "1")
        self.assertEqual(entry.get(), "100")
        self.assertEqual(var.get(), 100)
        self.assertEqual(entry._set_index("1", "1", "00"), 3)

    def test_index_w_del(self):
        var = tk.DoubleVar()
        entry = FloatRangeEntry(self.gui, minval=0, maxval=500, var=var)
        entry.invalidfunction("100", "1", "00", "1", "focusout", "0")
        self.assertEqual(entry.get(), "1")
        self.assertEqual(var.get(), 1)
        self.assertEqual(entry._set_index("1", "0", "0"), 1)

    def test_zero_turns_to_negativesign(self):
        entry = FloatRangeEntry(self.gui, minval=0, maxval=1)
        entry.invalidfunction("0", "0-", "-", "1", "key", "1")
        self.assertEqual(entry.get(), "-")

    def test_negative_turns_zero_wsign(self):
        entry = FloatRangeEntry(self.gui, minval=0, maxval=1)
        entry.invalidfunction("-", "--", "-", "1", "key", "1")
        self.assertEqual(entry.get(), "0")

    def test_positive_turns_negative_nonzero(self):
        entry = FloatRangeEntry(self.gui, minval=0, maxval=2)
        entry.invalidfunction("1", "1-", "-", "1", "key", "1")
        self.assertEqual(entry.get(), "-1")

    def test_negative_turns_positive_nonzero(self):
        entry = FloatRangeEntry(self.gui, minval=0, maxval=2)
        entry.invalidfunction("-1", "-1-", "-", "2", "key", "1")
        self.assertEqual(entry.get(), "1")

    def test_minval_if_empty(self):
        entry = FloatRangeEntry(self.gui, minval=1.2, maxval=2)
        entry.invalidfunction("", "", "", "0", "focusout", "-1")
        self.assertEqual(entry.get(), "1.2")

    def test_invalidcall(self):
        entry = FloatRangeEntry(self.gui, minval=1.2, maxval=2)
        with self.assertRaises(Exception):
            entry.invalidfunction("", "", "", "0", "abc", "-1")

    def test_int_ignores_dot(self):
        var = tk.IntVar()
        var.set(1)
        entry = IntRangeEntry(self.gui, minval=0, maxval=5, var=var)
        entry.invalidfunction("1", "1.", ".", "1", "key", "1")
        self.assertEqual(entry.get(), "1")

    def test_intscale_rounds(self):
        var = tk.DoubleVar()
        var.set(1.2)
        entry = FloatRangeEntry(self.gui, minval=0, maxval=5, var=var)
        scale = IntScale(self.gui, entry=entry)
        scale.set(1.5)
        self.assertEqual(scale.get(), 2)
        self.assertEqual(entry.get(), "2")
        self.assertEqual(var.get(), 2)
        scale.set(3)
        self.assertEqual(scale.get(), 3)
        self.assertEqual(entry.get(), "3")
        self.assertEqual(var.get(), 3)
