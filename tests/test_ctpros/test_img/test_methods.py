import unittest

from ctpros.img.classes import *


class TestSet_Classification(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1

    def setUp(self):
        img = NDArray((5, 5, 5, 5), verbosity=1)
        img.ravel()[:] = np.arange(img.size)
        self.img = img

    def test_threshold(self):
        mask = self.img.classify.threshold(5 ** 4 / 2, inplace=False)
        self.assertEqual(mask.sum(), 312)
        self.img.classify.threshold(5 ** 4 / 2)
        self.np.assert_equal(self.img, mask)

    def test_otsu_global(self):
        mask, _ = self.img.classify.otsu_global(1, inplace=False)
        self.assertEqual(mask.sum(), 314)
        self.img.classify.otsu_global(1)
        self.np.assert_equal(self.img.sum(), 314)

    def test_canny_edge(self):
        ndimg = NDArray((5, 5, 5), verbosity=0)
        ndimg[:] = 0
        ndimg[1:4, 1:4, 1:4] = 1
        edges = ndimg.classify.canny_edge(inplace=False)
        soln = copy.deepcopy(ndimg)
        soln[2, 2, 2] = 0
        self.np.assert_equal(soln, edges)

    def tearDown(self):
        pass


class TestSet_Filters(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1

    def setUp(self):
        img = NDArray((5, 5, 5, 5))
        img.ravel()[:] = np.arange(img.size)
        self.img = img

    def test_gauss(self):
        self.img.filter.gauss(1, 1)
        self.assertAlmostEqual(self.img.ravel()[-1], 581.2452954264534)

    def test_gauss_returns_sametype(self):
        self.img = self.img.astype(np.uint8)
        self.img.filter.gauss(1, 1)
        self.assertEqual(self.img.ravel()[-1], 137)
        self.assertEqual(self.img.dtype, np.uint8)

    def test_inv(self):
        self.img.filter.inv(10, self.img.filter.gauss, 1, 1)
        self.assertEqual(self.img.ravel()[-1], 1051.5470457354663)

    def test_mean(self):
        self.img.filter.mean(3)
        self.assertEqual(self.img.ravel()[-1], 572.0)

    def tearDown(self):
        pass


class TestSet_Generic(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1

    def setUp(self):
        img = NDArray((3, 4, 5))
        img.ravel()[:] = np.arange(img.size)
        self.img = img

    def tearDown(self):
        pass

    def test_tra(self):
        self.img.affine.scale(2)
        self.np.assert_equal(self.img.generic.tra(), self.img[1])

    def test_tra_voi_default(self):
        self.img.affine.scale(2)
        self.img.position[:] = 0
        self.img.voi.pos[:] = 4
        self.img.voi.shape[:] = 2
        self.img.voi.elsize[:] = 2
        expected = self.img[0, 2:4, 2:4]
        tra = self.img.generic.tra(voi=True)
        self.np.assert_equal(tra, expected)

    def test_tra_voi_newvoi(self):
        self.img.affine.scale(2)
        tra = self.img.generic.tra(
            pos=4, voi=VOI(pos=(2, 2, 2), elsize=[2, 2, 2], shape=(2, 2, 2))
        )
        expected = self.img[2, 1:3, 1:3]
        self.np.assert_equal(tra, expected)

    def test_cor(self):
        self.img.affine.scale(2)
        self.img.position[:] = 2 * self.img.position
        expected = np.flipud(self.img[:, 2, :])
        self.np.assert_equal(self.img.generic.cor(), expected)

    def test_cor_voi_default(self):
        self.img.affine.scale(2)
        self.img.position[:] = 1
        self.img.voi.pos[:] = 0
        self.img.voi.shape[:] = 2
        self.img.voi.elsize[:] = 2
        expected = np.flipud(self.img[0:2, 1, 0:2])

        cor = self.img.generic.cor(voi=True)
        self.np.assert_equal(cor, expected)

    def test_cor_voi_newvoi(self):
        self.img.affine.scale(2)
        cor = self.img.generic.cor(
            pos=1, voi=VOI(pos=[0, 0, 0], elsize=[2, 2, 2], shape=[2, 2, 2])
        )
        expected = np.flipud(self.img[0:2, 1, 0:2])
        self.np.assert_equal(cor, expected)

    def test_sag(self):
        self.img.affine.scale(2)
        self.img.position[:] = 2 * self.img.position
        sag = self.img.generic.sag()
        expected = self.img[:, :, 2].T
        self.np.assert_equal(sag, expected)

    def test_sag_voi_default(self):
        self.img.affine.scale(2)
        self.img.position[:] = 2
        self.img.voi.pos[:] = 0
        self.img.voi.shape[:] = 2
        self.img.voi.elsize[:] = 2
        expected = self.img[0:2, 0:2, 1].T

        sag = self.img.generic.sag(voi=True)
        self.np.assert_equal(sag, expected)

    def test_sag_voi_newvoi(self):
        self.img.affine.scale(2)
        sag = self.img.generic.sag(
            pos=2, voi=VOI(pos=[0, 0, 0], elsize=[2, 2, 2], shape=[2, 2, 2])
        )
        expected = self.img[0:2, 0:2, 1].T
        self.np.assert_equal(sag, expected)


class TestSet_Numeric(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1

    def setUp(self):
        img = NDArray((5, 5, 5))
        img.ravel()[:] = np.arange(img.size)
        self.img = img

    def tearDown(self):
        pass

    def test_second_moment_w_rotation(self):
        self.img.classify.threshold(0)
        self.img[:] = 0
        h, w = 2, 3
        self.img[:, 1 : 1 + h, 1 : 1 + w] = 1
        scale = 2
        theta = np.pi / 3
        self.img.affine.scale(scale)
        self.img.affine.rotate(theta, 0, 0)
        Isma = self.img.numeric.second_moment()
        expected_Ixx = (1 ** 2 * 4) * scale ** 4  # 4 pixels a distance 1 away
        expected_Iyy = (0.5 ** 2 * 6) * scale ** 4  # 6 pixels distance .5 away
        expected_Ixy = 0
        expected_I = np.array(
            [[expected_Ixx, expected_Ixy], [expected_Ixy, expected_Iyy]]
        )
        R = AffineTensor(2).rotate(theta)[:-1, :-1]
        rotated_expected_I = np.dot(R.copy().inv(), np.dot(expected_I, R))

        self.np.assert_equal(rotated_expected_I, Isma)
        self.assertTrue(Isma[0, 1] < 0)
        self.np.assert_almost_equal(Isma[0, 1], Isma[1, 0])


class TestSet_Registration(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1
        self.aim3name = os.path.sep.join(["data", "input", "test_v3.aim;1"])
        self.aim3twist = os.path.sep.join(["data", "input", "test_v3_twistedcrop.aim"])

    def setUp(self):
        self.target = ImgTemplate(self.aim3name, verbosity=1).load()
        self.reference = ImgTemplate(self.aim3twist, verbosity=1).load()

    def test_true3D(self):
        reference = self.target[1:, 1:, 2:]
        reference = self.target[:]
        reference.affine.rotate(0.2, 0, 0.1)
        self.target.register.true3D(reference, niterations=10)
        self.np.assert_almost_equal(
            self.target.affine.rotate(), [0.2, 0, 0.1], decimal=1
        )


class TestSet_Transform(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1

    def setUp(self):
        img = NDArray((5, 5, 5))
        array = np.array([0, 1, 2, 3, 4]).reshape((-1, 1, 1))
        img[:] = array
        self.img = img

    def test_affine_default(self):
        self.img.affine.translate(1, 0, 0)
        newimg = self.img.transform.affine(inplace=False)
        self.np.assert_equal(newimg[1:], self.img[:-1])
        self.np.assert_equal(newimg[0], 0)
        self.assertEqual(newimg.dtype, self.img.dtype)
        self.assertEqual(type(newimg), type(self.img))

    def test_affine_affine(self):
        newimg = self.img.transform.affine(
            AffineTensor(3).translate(1, 0, 0), inplace=False
        )
        self.np.assert_equal(newimg[1:], self.img[:-1])
        self.np.assert_equal(newimg[0], 0)
        self.assertEqual(newimg.dtype, self.img.dtype)
        self.assertEqual(type(newimg), type(self.img))

    def test_affine_voi(self):
        voi = VOI(shape=(4, 4, 4), elsize=(0.25, 0.25, 0.25), pos=(1.5, 0, 0))
        newimg = self.img.transform.affine(affine=None, voi=voi, inplace=False)

        self.np.assert_equal(
            newimg, np.ones((4, 4, 4)) * np.arange(1.5, 2.5, 0.25).reshape(-1, 1, 1)
        )
        self.assertEqual(newimg.dtype, self.img.dtype)
        self.assertEqual(type(newimg), type(self.img))

    def test_affine_nearest(self):
        newimg = self.img.transform.affine(
            AffineTensor(3).translate(0.6, 0, 0), interp=0, inplace=False
        )
        self.np.assert_equal(newimg[1:], self.img[:-1])
        self.np.assert_equal(newimg[0], 0)
        self.assertEqual(newimg.dtype, self.img.dtype)
        self.assertEqual(type(newimg), type(self.img))

    def test_affine_dtype(self):
        newimg = self.img.transform.affine(interp=0, dtype=np.uint32)
        self.np.assert_equal(self.img, newimg)
        self.assertEqual(newimg.dtype, np.uint32)

    def test_affine_dtype_bool(self):
        newimg = (self.img / self.img.max()).transform.affine(interp=0, dtype=np.bool_)
        self.assertTrue(newimg[3:].all())
        self.assertFalse(newimg[:3].any())

    def test_distance(self):
        self.img.affine.scale(2, 3, 4)
        self.img.transform.distance()
        for i in range(len(self.img)):
            self.np.assert_equal(self.img[i], i * 2)

    def tearDown(self):
        pass
