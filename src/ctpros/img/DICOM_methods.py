import os
import SimpleITK as sitk


class DICOM_methods():
    @staticmethod
    def rescale_dicom_folder(dicom_folder, output_folder, downscale_ratio_x=0.5, downscale_ratio_y=0.5, downscale_ratio_z=0.5, new_origin=(0, 0, 0), interp_order=3):
        # Get the list of DICOM files in the folder
        reader = sitk.ImageSeriesReader()
        dicom_names = reader.GetGDCMSeriesFileNames(dicom_folder)
        reader.SetFileNames(dicom_names)

        # Read the DICOM series
        image = reader.Execute()

        # Get the current spacing and size
        original_spacing = image.GetSpacing()
        original_size = image.GetSize()

        # Compute the new spacing based on the downscale ratio
        new_spacing = [original_spacing[0] / downscale_ratio_x, 
                    original_spacing[1] / downscale_ratio_y,
                    original_spacing[2] / downscale_ratio_z]

        # Compute the new size based on the downscale ratio
        new_size = [int(original_size[0] * downscale_ratio_x),
                    int(original_size[1] * downscale_ratio_y),
                    int(original_size[2] * downscale_ratio_z)]

        # Create a ResampleImageFilter
        resampler = sitk.ResampleImageFilter()

        # Set the resampler parameters
        resampler.SetOutputSpacing(new_spacing)
        resampler.SetSize(new_size)
        resampler.SetOutputDirection(image.GetDirection())
        
        # Set the new origin
        resampler.SetOutputOrigin(new_origin)

        resampler.SetTransform(sitk.Transform())
        resampler.SetDefaultPixelValue(image.GetPixelIDValue())

        # Set interpolation method based on interp_order
        if interp_order == 0:
            resampler.SetInterpolator(sitk.sitkNearestNeighbor)
        elif interp_order == 1:
            resampler.SetInterpolator(sitk.sitkLinear)
        elif interp_order == 3:
            resampler.SetInterpolator(sitk.sitkBSpline)
        else:
            raise ValueError(f"Unsupported interpolation order: {interp_order}")

        # Resample the image
        rescaled_image = resampler.Execute(image)

        # Cast the pixel type to signed 16-bit integer
        caster = sitk.CastImageFilter()
        caster.SetOutputPixelType(sitk.sitkInt16)
        rescaled_image = caster.Execute(rescaled_image)

        # Write the rescaled DICOM series
        writer = sitk.ImageFileWriter()
        for i in range(rescaled_image.GetDepth()):
            image_slice = rescaled_image[:,:,i]
            writer.SetFileName(os.path.join(output_folder, f"rescaled_slice_{i}.dcm"))
            writer.Execute(image_slice)

    # Run the function
    rescale_dicom_folder("/path/to/dicom_folder", "/path/to/output_folder", 0.5, 0.7, 0.5, (10, 10, 10), 3)
