# GUI Controls Documentation

## Positioning
Positioning within the image refers to the point in the volume which the 2D cross-sections are defined.
- **[Mouse Left Click/Hold]** = Sets the image position to the mouse position
- **[Double Mouse Left Click]** = Resets the position to the middle of the primary image
- **[Ctrl]**+**[Shift]**+**[T]** = Toggle View of Crosshairs
## Translation
Translation refers to the offset that the image is placed at relative to the origin.
- **[Mouse Right Hold]** = Drags the image with the mouse to perform the translation
- **[Double Mouse Right Click]** = Resets the orientation of the primary image
## Rotation
Rotation occurs in 2D along the cross-section selected and is performed about the crosshairs.
- **[Shift]**+**[Mouse Right Hold]** = Rotates the cross-section based on the angle from the cross-section as the mouse is dragged
- **[Double Mouse Right Click]** = Resets the orientation of the primary image
## VOI Controls
The VOI defines a subvolume of the image often used for cropping or selectively applying certain 
- **[Alt]**+**[Mouse Left Click/Hold]** = Sets the origin corner of the VOI to the mouse position
- **[Alt]**+**[Mouse Right Click/Hold]** = Sets the far corner of the VOI to the mouse position
- **[Ctrl]**+**[Shift]**+**[V]** = Toggle View of VOI
- **[Ctrl]**+**[Shift]**+**[Z]** = Toggle Zoom to VOI

## Notes
- When performing some computational tasks, the program may seem unresponsive. This behavior should be expected;
  clear visualization that the program is processing is in progress.

## Return to [README](../README.md)