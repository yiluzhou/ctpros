import setuptools, glob, os, sys

version = "0.1.4dev"
author = "Carlos Osuna, Yilu Zhou"
author_email = "charlie@caosuna.com, yiluzhou1987@gmail.com"
ldct = "text/markdown"
url = "https://gitlab.com/yiluzhou/ctpros"
dependencies = [
        'black', 'coverage', 'coverage-badge', 'twine', 'sphinx', 'pydata-sphinx-theme', 
        'itk', 'mayavi', 'numpy', 'pandas', 'PyQt5-Qt5', 'scipy', 'scikit-image', 'tqdm', 
        'xlrd', 'vtk', 'chardet', 'nibabel', 'SimpleITK', 'pydicom',
        'lazy_import @ https://github.com/yiluzhou/lazy_import/archive/refs/heads/master.zip',
    ]

setuptools.setup(
    name="ctpros",
    version=version,
    author=author,
    author_email=author_email,
    description="Computed Tomography Processing Registration Open Sourced (based on ctpros by Original Author Carlos Osuna)",
    long_description="# ctpros: Computed Tomography Processing & Registration - Open Sourced\nSee the [project homepage](https://gitlab.com/yiluzhou/ctpros) for details.",
    long_description_content_type=ldct,
    url=url,
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=dependencies,
    python_requires=">=3.10",
)
